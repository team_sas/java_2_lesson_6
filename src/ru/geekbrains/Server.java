package ru.geekbrains;

import java.io.*;
import java.net.*;

public class Server {

    private final int SERVER_PORT = 8189;

    BufferedReader console;

    Socket clientSocket;
    ServerSocket serverSocket;

    DataInputStream in;
    DataOutputStream out;

    private boolean isSwitchOff = false;
    private boolean connectionEstablished = false;

    /**
     * Запуск сервера.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        Server server = new Server();
        server.catchClient();
        server.startMessaging();
    }

    Server() throws IOException {
        console = new BufferedReader(new InputStreamReader(System.in));
        serverSocket = new ServerSocket(SERVER_PORT);
        this.runCliListener();
        System.out.println("Сервер успешно запущен. Для остановки отправьте команду /stop");
    }

    /**
     * Ожидает подключения клиента к серверу.
     */
    private void catchClient() {
        System.out.println("Сервер ожидает подключение клиента.");
        try {
            clientSocket = serverSocket.accept();
            in = new DataInputStream(clientSocket.getInputStream());
            out = new DataOutputStream(clientSocket.getOutputStream());
            connectionEstablished = true;
        } catch (IOException e) {
            System.out.println("Подключение клиента невозможно. Ошибка: " + e.getMessage());
            return;
        }
        System.out.println("К серверу подключился клиент.");
    }

    /**
     * Запускает процес чтения сообщений из командной строки.
     * При получении из командной строки сообщения /stop работа сервера будет завершена.
     */
    private void runCliListener() {
        Server server = this;
        Thread consoleListenerThread = new Thread() {
            public void run() {
                String message;
                while (true) {
                    try {
                        message = console.readLine().trim();
                    } catch (IOException e) {
                        System.out.println("Не удалось прочитать строку из консоли. Ошибка: " + e.getMessage());
                        continue;
                    }
                    if (message.trim().equalsIgnoreCase("/stop")) server.stop();
                    if (message.isEmpty()) continue;
                    if (!connectionEstablished) {
                        System.out.println("Ваше сообщение не может быть отправлено, т.к. клиент не подключен!");
                        continue;
                    }
                    try {
                        out.writeUTF(message);
                    } catch (IOException e) {
                        System.out.println("Не удалось отправить сообщение клиенту. Ошибка: " + e.getMessage());
                    }
                }
            }
        };
        try {
            consoleListenerThread.join();
            consoleListenerThread.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Запускает процесс получения сообщений от клиента с выводом их в командную строку.
     * В случае если во время обмена сообщениями произойдет потеря соединения с клиентом сервер перейдет в режим
     * ожидания подключения клиента.
     */
    private void startMessaging() {
        Thread clientMessagesThread = new Thread() {
            public void run() {
                while (true) {
                    if (!connectionEstablished) {
                        try {
                            this.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        continue;
                    }
                    try {
                        String clientMessage = in.readUTF();
                        System.out.println("Клиент: " + clientMessage);
                    } catch (IOException e) {
                        if (isSwitchOff) break;
                        System.out.println("Потеряна связь с клиентом...");
                        connectionEstablished = false;
                        catchClient();
                        startMessaging();
                        break;
                    }
                }
            }
        };
        try {
            clientMessagesThread.join();
            clientMessagesThread.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Запущен процесс обмена сообщениями между сервером и клиентом.");
    }

    /**
     * Завершение работы сервера
     */
    private void stop() {
        isSwitchOff = true;
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (connectionEstablished) {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Сервер остановлен!");
        System.exit(0);
    }
}
