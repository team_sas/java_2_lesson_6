package ru.geekbrains;

import java.io.*;
import java.net.*;

public class Client {

    private final int SERVER_PORT = 8189;
    private final String SERVER_HOST = "localhost";

    /**
     * Время в милисекундах между попытками подключения к серверу.
     */
    private final int CONNECT_TO_SERVER_TIMEOUT = 30000;

    /**
     * Количество попыток подключения к серверу.
     */
    private final int CONNECT_TO_SERVER_ATTEMPTS = 3;

    Socket socket;

    DataInputStream in;
    DataOutputStream out;

    BufferedReader console;

    private boolean isSwitchOff = false;
    private boolean connectionEstablished = false;

    /**
     * Запуск клиента.
     * @param args
     */
    public static void main(String[] args) {
        Client client = new Client();
        client.connectToServer();
        client.readServerMessages();
    }

    private Client() {
        console = new BufferedReader(new InputStreamReader(System.in));
        this.runCliListener();
        System.out.println("Клиент успешно запущен. Для выхода отправьте команду /exit");
    }

    /**
     * Подключение к серверу. Если подключение не удалось, будет произведено еще несколько попыток подключения
     * с заданным интервалом.
     * @see CONNECT_TO_SERVER_ATTEMPTS
     * @see CONNECT_TO_SERVER_TIMEOUT
     */
    private void connectToServer() {
        System.out.println("Подключение к серверу...");
        Thread connectToServerThread = new Thread() {
            public void run() {
                int attempts = CONNECT_TO_SERVER_ATTEMPTS;
                while (true) {
                    try {
                        socket = new Socket(SERVER_HOST, SERVER_PORT);
                        in = new DataInputStream(socket.getInputStream());
                        out = new DataOutputStream(socket.getOutputStream());
                        connectionEstablished = true;
                        System.out.println("Клиент успешно подключен к серверу.");
                        break;
                    } catch (IOException e) {
                        if (attempts == 0) {
                            System.out.println("Соединение с сервером не установлено!");
                            exit();
                            break;
                        }
                        attempts--;
                        System.out.println("Не удалось подключится к серверу, следующая попытка через 30 сек.");
                        try {
                            this.sleep(CONNECT_TO_SERVER_TIMEOUT);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
        };
        try {
            connectToServerThread.join();
            connectToServerThread.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Запускает процес чтения сообщений из командной строки.
     * При получении из командной строки сообщения /exit работа клиента будет завершена.
     */
    private void runCliListener() {
        Client client = this;
        Thread consoleListenerThread = new Thread() {
            public void run() {
                String message;
                while (true) {
                    try {
                        message = console.readLine().trim();
                    } catch (IOException e) {
                        System.out.println("Не удалось прочитать строку из консоли. Ошибка: " + e.getMessage());
                        continue;
                    }
                    if (message.trim().equalsIgnoreCase("/exit")) client.exit();
                    if (message.isEmpty()) continue;
                    if (!connectionEstablished) {
                        System.out.println(
                            "Ваше сообщение не может быть отправлено, т.к. отсутствует подключение к серверу!"
                        );
                        continue;
                    }
                    try {
                        out.writeUTF(message);
                    } catch (IOException e) {
                        System.out.println("Не удалось отправить сообщение серверу. Ошибка: " + e.getMessage());
                    }
                }
            }
        };
        try {
            consoleListenerThread.join();
            consoleListenerThread.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Запускает процесс получения сообщений с сервера и вывод их в командную строку.
     * В случае если во время обмена сообщениями произойдет потеря соединения с сервером будет произведена
     * попытка установить соединение.
     */
    private void readServerMessages() {
        Thread serverMessagesThread = new Thread() {
            public void run() {
                boolean isMessagingStart = false;
                while (true) {
                    if (!connectionEstablished) {
                        try {
                            this.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        continue;
                    }
                    if (!isMessagingStart) {
                        System.out.println("Запущен процесс обмена сообщениями между клиентом и сервером.");
                        isMessagingStart = true;
                    }
                    try {
                        String serverMessage = in.readUTF();
                        System.out.println("Сервер: " + serverMessage);
                    } catch (IOException e) {
                        if (isSwitchOff) break;
                        System.out.println("Связь с сервером потеряна...");
                        connectionEstablished = false;
                        connectToServer();
                        readServerMessages();
                        break;
                    }
                }
                return;
            }
        };
        try {
            serverMessagesThread.join();
            serverMessagesThread.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Завершение работы клиента.
     */
    void exit() {
        isSwitchOff = true;
        if (connectionEstablished) {
            try {
                socket.close();
            } catch (IOException e) {}
            try {
                out.close();
            } catch (IOException e) {}
            try {
                in.close();
            } catch (IOException e) {}
        }
        System.out.println("Работа клиента завершена.");
        System.exit(0);
    }
}
